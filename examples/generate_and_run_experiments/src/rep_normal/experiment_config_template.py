##
## This file is part of the exputils package.
##
## Copyright: INRIA
## Year: 2022, 2023
## Contact: chris.reinke@inria.fr
##
## exputils is provided under GPL-3.0-or-later
##
def get_config():

    config = dict()

    config['seed'] = 468413 + <repetition_id>

    config['n_steps'] = <n_steps>

    config['init_value'] = <init_value>
    config['force'] = <force>
    config['sigma'] = <sigma>

    return config
